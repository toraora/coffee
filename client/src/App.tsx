import { h, render } from "preact";
import { Provider, createClient, Query } from "urql";

const client = createClient({
  url: "http://localhost:3000/graphql"
});

const getReviews = `
  query MyQuery {
    allReviewsList {
      id
      date
      username
      grinder
      rating
    }
  }
`;

const App = () => (
  <Provider value={client}>
    <Query query={getReviews}>
      {({ fetching, data, error, extensions }) => {
        if (fetching) {
          return <p>Loading</p>;
        }

        return <p>{data.allReviewsList[0].grinder}</p>;
      }}
    </Query>
  </Provider>
);

render(<App />, document.body);

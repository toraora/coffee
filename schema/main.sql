DROP DATABASE IF EXISTS coffee;
DROP ROLE IF EXISTS coffee_admin;

-- create role now and switch so that everything created is owned by this role
CREATE ROLE coffee_admin SUPERUSER LOGIN PASSWORD 'coffee';
SET ROLE coffee_admin;

CREATE DATABASE coffee;
\c coffee;

-- needed for UUIDv4 generation
CREATE EXTENSION "pgcrypto";

-- SCHEMA public is accessible to everyone by default
REVOKE ALL ON SCHEMA public FROM PUBLIC;


\i review.sql
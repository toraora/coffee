CREATE TABLE review (
    id                  UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    --is_deleted          BOOLEAN DEFAULT false,

    user_id             TEXT, -- foreign key

    date                TIMESTAMPTZ,

    grinder             TEXT,
    grind_setting       TEXT,

    brew_method         TEXT,
    water_temp          TEXT,

    roaster             TEXT,
    roast_date          TIMESTAMPTZ,
    roast_name          TEXT,
    roast_origin        TEXT,

    coffee_amount       TEXT,
    water_amount        TEXT,

    rating              INT,
    extraction_time     TEXT,
    extraction_amount   TEXT,

    notes               TEXT
);

INSERT INTO review (
    username,
    date,
    grinder,
    grind_setting,
    rating
)
VALUES (
    'nathan',
    now(),
    'Kinu M47 Phoenix',
    '42',
    3
);
const express = require("express");
const { postgraphile } = require("postgraphile");

const app = express();

app.use(
  postgraphile(
    process.env.DATABASE_URL ||
      "postgres://coffee_admin:coffee@localhost:5432/coffee",
    "public",
    {
      watchPg: true,
      graphiql: true,
      enhanceGraphiql: true,
      enableCors: true,
      simpleCollections: "both"
    }
  )
);

app.listen(process.env.PORT || 3000);
